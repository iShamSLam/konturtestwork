package com.rishat.shamsutdinov.konturtestwork.presentation.contactList

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rishat.shamsutdinov.konturtestwork.Utils.Constant.LAST_PULL_TIME
import com.rishat.shamsutdinov.konturtestwork.Utils.isDifferenceInMinuteLessThan
import com.rishat.shamsutdinov.konturtestwork.data.repository.ContactRepository
import com.rishat.shamsutdinov.konturtestwork.entity.Contact
import com.rishat.shamsutdinov.konturtestwork.errors.Error
import com.rishat.shamsutdinov.konturtestwork.events.OnContactsReceived
import com.rishat.shamsutdinov.konturtestwork.events.OnDataPulledFromNet
import com.rishat.shamsutdinov.konturtestwork.events.OnNetworkError
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.time.OffsetDateTime
import java.util.*

class ContactListViewModel(
    private val repository: ContactRepository,
    private val sharedPreferences: SharedPreferences
) : ViewModel() {

    private val _contacts = MutableLiveData<List<Contact>>()
    val contacts: LiveData<List<Contact>> = _contacts

    private val _isDataDownloading = MutableLiveData(true)
    val isDataDownDownloading: LiveData<Boolean> = _isDataDownloading

    private val _currentError = MutableLiveData<Error>()
    val currentError: LiveData<Error> = _currentError

    private lateinit var immutableContact: List<Contact>
    private val difference = 1

    init {
        EventBus.getDefault().register(this)
        with(Calendar.getInstance()) {
            OffsetDateTime.now().also {
                set(it.year, it.monthValue, it.dayOfMonth, it.hour, it.minute)
            }
            if (this.isDifferenceInMinuteLessThan(getLastPullInfo(), difference)) {
                repository.fetchFromDataBase()
            } else {
                repository.pullDataFromNet()
            }
        }
    }

    private fun getLastPullInfo(): Calendar {
        val result = Calendar.getInstance()
        sharedPreferences.getString(LAST_PULL_TIME, "")?.let {
            if (it.isNotBlank()) {
                OffsetDateTime.parse(it)
                    ?.let { date ->
                        result.set(
                            date.year,
                            date.monthValue,
                            date.dayOfMonth,
                            date.hour,
                            date.minute
                        )
                    }
            }
        }

        return result
    }

    @Subscribe
    fun onContactsReceived(event: OnContactsReceived) {
        immutableContact = event.contacts
        _contacts.value = immutableContact
        _isDataDownloading.value = false
    }

    fun searchContactsByName(name: String) {
        if (name.isNotBlank()) {
            if (name.matches(Regex(".*\\d.*"))) {
                _contacts.value = immutableContact.filter {
                    it.phone.filter { char -> char.isDigit() }.contains(name)
                }
            } else {
                _contacts.value =
                    immutableContact.filter { it.name.toLowerCase(Locale.ENGLISH).contains(name) }
            }
        } else {
            _contacts.value = immutableContact
        }
    }

    @Subscribe
    fun onDataPulledFromNet(event: OnDataPulledFromNet) {
        with(OffsetDateTime.now()) {
            sharedPreferences.edit().putString(LAST_PULL_TIME, this.toString()).apply()
        }
    }

    @Subscribe
    fun onNetworkError(event: OnNetworkError) {
        _currentError.value = Error.SimpleNetworkError()
    }

    fun refreshData() {
        _isDataDownloading.value = true
        repository.pullDataFromNet()
    }

    override fun onCleared() {
        EventBus.getDefault().unregister(this)
        super.onCleared()
    }
}