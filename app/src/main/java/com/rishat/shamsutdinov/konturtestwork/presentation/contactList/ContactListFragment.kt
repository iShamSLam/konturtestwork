package com.rishat.shamsutdinov.konturtestwork.presentation.contactList

import android.app.Activity
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.rishat.shamsutdinov.konturtestwork.R
import com.rishat.shamsutdinov.konturtestwork.Utils.setEnabledAllChild
import com.rishat.shamsutdinov.konturtestwork.databinding.ContactListFragmentBinding
import com.rishat.shamsutdinov.konturtestwork.errors.Error
import com.rishat.shamsutdinov.konturtestwork.events.OnContactClicked
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactListFragment : Fragment() {

    private val viewModel by viewModel<ContactListViewModel>()
    private val adapter = ContactListAdapter()
    private var _binding: ContactListFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    private val activity: Activity
        get() = this.getActivity() as Activity

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe()
    fun onContactClicked(event: OnContactClicked) {
        Navigation.findNavController(activity, R.id.navHostFragment).navigate(ContactListFragmentDirections.actionListToDetails(event.id))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvContacts.adapter = adapter
        binding.rvContacts.layoutManager = LinearLayoutManager(context)
        binding.srlRefresh.setOnRefreshListener {
            viewModel.refreshData()
            binding.srlRefresh.isRefreshing = false
        }
        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    viewModel.searchContactsByName(newText)
                }
                return false
            }

        })
        viewModel.contacts.observe(viewLifecycleOwner, {
            adapter.data = it
        })
        viewModel.isDataDownDownloading.observe(viewLifecycleOwner, {
            when (it) {
                true -> {
                    binding.pgDownloading.visibility = View.VISIBLE
                    binding.rvContacts.visibility = View.INVISIBLE
                }
                false -> {
                    binding.pgDownloading.visibility = View.INVISIBLE
                    binding.rvContacts.visibility = View.VISIBLE
                }
            }
            binding.search.setEnabledAllChild(!it)
        })
        viewModel.currentError.observe(viewLifecycleOwner, {
            when (it) {
                is Error.SimpleNetworkError -> Snackbar.make(view, it.message, Snackbar.LENGTH_SHORT).show()
            }
        })
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = ContactListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}