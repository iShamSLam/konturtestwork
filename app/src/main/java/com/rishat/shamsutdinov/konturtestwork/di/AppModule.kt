package com.rishat.shamsutdinov.konturtestwork.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.google.gson.Gson
import com.rishat.shamsutdinov.konturtestwork.R
import com.rishat.shamsutdinov.konturtestwork.data.ContactDao
import com.rishat.shamsutdinov.konturtestwork.data.ContactDatabase
import com.rishat.shamsutdinov.konturtestwork.data.repository.ContactRepository
import com.rishat.shamsutdinov.konturtestwork.data.repository.ContactRepositoryImplementation
import com.rishat.shamsutdinov.konturtestwork.presentation.ContactDetails.ContactDetailsViewModel
import com.rishat.shamsutdinov.konturtestwork.presentation.contactList.ContactListViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val okHttpModule = module {
    single {
        OkHttpClient()
    }
}

val gsonModule = module {
    single {
        Gson()
    }
}

val databaseModule = module {
    fun provideDatabase(application: Application): ContactDatabase {
        return Room.databaseBuilder(application, ContactDatabase::class.java, "contactdb")
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideContactDao(database: ContactDatabase): ContactDao {
        return database.contactDao()
    }

    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            context.getString(R.string.last_pull_time),
            Context.MODE_PRIVATE
        )
    }

    single {
        provideDatabase(androidApplication())
    }
    single {
        provideContactDao(get())
    }
    single {
        provideSharedPreferences(androidApplication())
    }
}

val repositoryModule = module {
    single<ContactRepository> {
        ContactRepositoryImplementation(get(), get(), get())
    }
}

val viewModelModule = module {
    viewModel {
        ContactListViewModel(get(), get())
    }
    viewModel {
        ContactDetailsViewModel(get())
    }
}