package com.rishat.shamsutdinov.konturtestwork.errors

import com.rishat.shamsutdinov.konturtestwork.R

sealed class Error {
    data class SimpleNetworkError(val message: Int = R.string.simple_network_error) : Error()
}