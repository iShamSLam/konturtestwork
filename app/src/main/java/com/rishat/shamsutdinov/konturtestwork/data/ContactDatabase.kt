package com.rishat.shamsutdinov.konturtestwork.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rishat.shamsutdinov.konturtestwork.entity.Contact
import com.rishat.shamsutdinov.konturtestwork.entity.EducationPeriod

@Database(entities = [Contact::class, EducationPeriod::class], version = 1)
@TypeConverters(DateConverter::class)
abstract class ContactDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
}