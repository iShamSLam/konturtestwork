package com.rishat.shamsutdinov.konturtestwork.presentation.ContactDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rishat.shamsutdinov.konturtestwork.data.repository.ContactRepository
import com.rishat.shamsutdinov.konturtestwork.entity.Contact
import com.rishat.shamsutdinov.konturtestwork.events.OnSingleContactReceived
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class ContactDetailsViewModel(private val repository: ContactRepository) : ViewModel() {

    private val _contact = MutableLiveData<Contact>()
    val contact: LiveData<Contact> = _contact

    init {
        EventBus.getDefault().register(this)
    }

    fun getContact(id: String) {
        repository.getContractById(id)
    }

    @Subscribe
    fun onSingleContactReceived(event: OnSingleContactReceived) {
        _contact.value = event.contact
    }

    override fun onCleared() {
        EventBus.getDefault().unregister(this)
        super.onCleared()
    }
}