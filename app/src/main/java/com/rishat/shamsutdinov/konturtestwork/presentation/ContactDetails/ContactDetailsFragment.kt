package com.rishat.shamsutdinov.konturtestwork.presentation.ContactDetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rishat.shamsutdinov.konturtestwork.R
import com.rishat.shamsutdinov.konturtestwork.databinding.ContactDetailsFragmentBinding
import com.rishat.shamsutdinov.konturtestwork.databinding.ContactListFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ContactDetailsFragment : Fragment() {

    private val viewModel by viewModel<ContactDetailsViewModel>()
    private var _binding: ContactDetailsFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = ContactDetailsFragmentArgs.fromBundle(requireArguments())
        viewModel.getContact(args.contactId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.contact.observe(viewLifecycleOwner, {
            binding.tvName.text = it.name
            binding.tvNumber.text = it.phone
            binding.tvTemperament.text = it.temperament?.name
            binding.tvBiography.text = it.biography
            with(SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)) {
                it.educationPeriod?.start?.let { start ->
                    it.educationPeriod.end?.let { end ->
                        binding.tvEducationPeriod.text = getString(
                            R.string.education_period,
                            this.format(start),
                            this.format(end)
                        )
                    }
                }
            }
        })
        context?.getColor(R.color.white)?.let { binding.ibBack.setColorFilter(it) }
        binding.ibBack.setOnClickListener {
            activity?.onBackPressed()
        }
        binding.tvNumber.setOnClickListener {
            with(Intent(Intent.ACTION_DIAL)) {
                data = Uri.parse("tel:${binding.tvNumber.text}")
                activity?.startActivity(this)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ContactDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}