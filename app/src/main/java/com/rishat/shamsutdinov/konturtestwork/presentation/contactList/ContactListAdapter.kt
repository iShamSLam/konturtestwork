package com.rishat.shamsutdinov.konturtestwork.presentation.contactList

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rishat.shamsutdinov.konturtestwork.R
import com.rishat.shamsutdinov.konturtestwork.Utils.inflate
import com.rishat.shamsutdinov.konturtestwork.databinding.ItemContactBinding
import com.rishat.shamsutdinov.konturtestwork.entity.Contact
import com.rishat.shamsutdinov.konturtestwork.events.OnContactClicked
import org.greenrobot.eventbus.EventBus

class ContactListAdapter : RecyclerView.Adapter<ContactListAdapter.ContactViewHolder>() {

    var data: List<Contact> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    inner class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(contact: Contact) = itemView.apply {
            with(ItemContactBinding.bind(this)) {
                this.tvName.text = contact.name
                this.tvHeight.text = contact.height.toString()
                this.tvNumber.text = contact.phone
                this.cvContact.setOnClickListener {
                    EventBus.getDefault().post(OnContactClicked(contact.id))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(parent.inflate(R.layout.item_contact))
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size
}