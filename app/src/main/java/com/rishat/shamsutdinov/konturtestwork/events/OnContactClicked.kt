package com.rishat.shamsutdinov.konturtestwork.events

data class OnContactClicked(
    val id: String
)