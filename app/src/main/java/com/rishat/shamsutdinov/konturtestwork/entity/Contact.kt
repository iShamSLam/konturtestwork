package com.rishat.shamsutdinov.konturtestwork.entity

import androidx.room.*
import com.google.gson.annotations.SerializedName
import com.rishat.shamsutdinov.konturtestwork.data.DateConverter
import java.util.*

@Entity
data class Contact(
    @PrimaryKey
    val id: String = "",
    val name: String = "",
    val height: Float = 0f,
    val phone: String = "",
    val biography: String = "",
    val temperament: Temperament? = null,
    @Embedded
    val educationPeriod: EducationPeriod? = null
)

enum class Temperament {
    @SerializedName("melancholic")
    Melancholic,

    @SerializedName("phlegmatic")
    Phlegmatic,

    @SerializedName("sanguine")
    Sanguine,

    @SerializedName("choleric")
    Choleric
}

@Entity
data class EducationPeriod(
    @PrimaryKey(autoGenerate = true)
    val periodId : Long,
    @TypeConverters(DateConverter::class)
    val start: Date? = null,
    @TypeConverters(DateConverter::class)
    val end: Date? = null
)
