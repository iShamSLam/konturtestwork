package com.rishat.shamsutdinov.konturtestwork.data.repository

import com.rishat.shamsutdinov.konturtestwork.entity.Contact

interface ContactRepository {
    fun fetchFromDataBase()
    fun getContractById(id: String)
    fun pullDataFromNet()
}