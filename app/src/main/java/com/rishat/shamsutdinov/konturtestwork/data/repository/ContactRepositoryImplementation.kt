package com.rishat.shamsutdinov.konturtestwork.data.repository

import com.google.gson.Gson
import com.rishat.shamsutdinov.konturtestwork.data.ContactDao
import com.rishat.shamsutdinov.konturtestwork.entity.Contact
import com.rishat.shamsutdinov.konturtestwork.events.OnContactsReceived
import com.rishat.shamsutdinov.konturtestwork.events.OnDataPulledFromNet
import com.rishat.shamsutdinov.konturtestwork.events.OnNetworkError
import com.rishat.shamsutdinov.konturtestwork.events.OnSingleContactReceived
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.greenrobot.eventbus.EventBus

class ContactRepositoryImplementation(
    private val okHttpClient: OkHttpClient,
    private val gson: Gson,
    private val dataBase: ContactDao
) : ContactRepository {


    override fun fetchFromDataBase() {
        dataBase.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.isNullOrEmpty()) {
                    pullDataFromNet()
                } else {
                    EventBus.getDefault().post(OnContactsReceived(it))
                }
            }
    }

    override fun getContractById(id: String) {
        dataBase.getContactById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                it.message
            }
            .subscribe {
                EventBus.getDefault().post(OnSingleContactReceived(it))
            }
    }

    override fun pullDataFromNet() {
        Observable.fromArray(links)
            .subscribeOn(Schedulers.io())
            .doOnComplete {
                EventBus.getDefault().post(OnDataPulledFromNet())
            }
            .map { url ->
                val result = mutableListOf<Response>()
                url.forEach {
                    result.add(
                        okHttpClient.newCall(
                            Request.Builder()
                                .url(it)
                                .build()
                        )
                            .execute()
                    )
                }
                return@map result
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    saveToDataBase(
                        response.map { item ->
                            gson.fromJson(
                                item.body?.string(),
                                Array<Contact>::class.java
                            )
                        }.toTypedArray()
                            .flatten()
                    )
                }, {
                    fetchFromDataBase()
                    EventBus.getDefault().post(OnNetworkError())
                }
            )
    }

    private fun saveToDataBase(list: List<Contact>) {
        dataBase.insertAll(list)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                fetchFromDataBase()
            }
    }

    companion object {
        val links = listOf(
            "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/generated-01.json",
            "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/generated-02.json",
            "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/generated-03.json"
        )
    }
}