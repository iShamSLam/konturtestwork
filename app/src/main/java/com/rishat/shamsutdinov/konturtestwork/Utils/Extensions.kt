package com.rishat.shamsutdinov.konturtestwork.Utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.forEach
import java.util.*

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

fun Calendar.isDifferenceInMinuteLessThan(anotherDate: Calendar, difference: Int): Boolean {
    return this.get(Calendar.YEAR) == anotherDate.get(Calendar.YEAR)
            && this.get(Calendar.MONTH) == anotherDate.get(Calendar.MONTH)
            && this.get(Calendar.DAY_OF_MONTH) == anotherDate.get(Calendar.DAY_OF_MONTH)
            && this.get(Calendar.HOUR) == anotherDate.get(Calendar.HOUR)
            && (this.get(Calendar.MINUTE) - anotherDate.get(Calendar.MINUTE) <= difference)
}

fun View.setEnabledAllChild(enabled: Boolean) {
    if (!enabled) {
        this.clearFocus()
    }
    isEnabled = enabled
    if (this is ViewGroup) {
        this.forEach {
            it.setEnabledAllChild(enabled)
        }
    }
}