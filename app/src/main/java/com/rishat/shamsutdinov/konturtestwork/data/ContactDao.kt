package com.rishat.shamsutdinov.konturtestwork.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rishat.shamsutdinov.konturtestwork.entity.Contact
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

@Dao
public interface ContactDao {
    @Query("SELECT * FROM contact")
    fun getAll(): Maybe<List<Contact>>

    @Query("SELECT * FROM contact WHERE id = :contactId")
    fun getContactById(contactId: String): Maybe<Contact>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(contacts: List<Contact>): Completable
}