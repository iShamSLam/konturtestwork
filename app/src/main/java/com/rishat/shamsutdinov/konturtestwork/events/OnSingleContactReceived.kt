package com.rishat.shamsutdinov.konturtestwork.events

import com.rishat.shamsutdinov.konturtestwork.entity.Contact

data class OnSingleContactReceived( val contact : Contact)
